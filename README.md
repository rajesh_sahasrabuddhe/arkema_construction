# Shiny Template #

This is the OWAC Shiny template

## Creating your own app from this template

1. Fork and rename this repository.
2. Edit this README to be appropriate for your new project.
3. Clone to your local computer and start customizing.

## Shiny App Basics

[Official documentation and tutorials](http://shiny.rstudio.com/)

Once you have this template app on your local computer, to run the template app just open the `global.R`, `ui.R`, or `server.R` file in Rstudio and click the "Run App" play button located in the top right of your code editor.